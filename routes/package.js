const express = require("express");

const { body } = require("express-validator");

const router = express.Router();

const feedController = require("../controllers/package");

router.get("/package/:id", feedController.getPackageById);

router.get("/category/:id", feedController.getCategoryId);

router.get("/categories", feedController.getCategories);

router.post(
  "/package",
  [
    body("title_en")
      .trim()
      .isLength({ min: 1 })
      .withMessage("English title is required"),
    body("title_ar")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Arabic title is required"),
    body("price")
      .isFloat({ gt: 0 })
      .withMessage("Price must be greater than 0"),
    body("features_en")
      .isArray()
      .withMessage("Features in English must be an array"),
    body("features_ar")
      .isArray()
      .withMessage("Features in Arabic must be an array"),
    body("categoryId").isMongoId().withMessage("Invalid category ID"),
  ],

  feedController.createPackages
);

router.post(
  "/categories",
  [
    body("name_en")
      .trim()
      .isLength({ min: 1 })
      .withMessage("English title is required"),
    body("name_ar")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Arabic title is required"),
  ],

  feedController.createCategory
);

module.exports = router;
