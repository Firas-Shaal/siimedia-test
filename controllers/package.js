const { validationResult } = require("express-validator");
const { Category, Package } = require("../models/package");

exports.createPackages = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation failed, entered data is incorrect");
      error.statusCode = 422;
      throw error;
    }
    const { title_en, title_ar, price, features_en, features_ar, categoryId } =
      req.body;

    const category = await Category.findById(categoryId);
    if (!category) {
      const error = new Error("This category don`t exist");
      error.statusCode = 422;
      throw error;
    }
    const newPackage = new Package({
      title_en,
      title_ar,
      price,
      features_en,
      features_ar,
      categoryId,
    });

    await newPackage.save();

    category.packages.push(newPackage._id);

    await category.save();

    res.status(201).json(newPackage);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getPackageById = async (req, res, next) => {
  try {
    const packageId = req.params.id;
    const package = await Package.findById(packageId);

    if (!package) {
      return res.status(404).json({ message: "Package not found" });
    }

    res.json(package);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.createCategory = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("Validation failed, entered data is incorrect");
      error.statusCode = 422;
      throw error;
    }
    const { name_en, name_ar } = req.body;
    const newCategory = new Category({ name_en, name_ar });
    await newCategory.save();
    res.status(201).json(newCategory);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getCategories = async (req, res, next) => {
  try {
    const categories = await Category.find().populate("packages");
    res.json(categories);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getCategoryId = async (req, res, next) => {
  try {
    const categoryId = req.params.id;
    const category = await Category.findById(categoryId).populate("packages");
    if (!category) {
      return res.status(404).json({ message: "Category not found" });
    }
    res.json(category);
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
